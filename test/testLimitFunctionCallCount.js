const limitFunctionCallCount = require("../limitFunctionCallCount");

function callBack(n) {
  console.log("callBack function is called.");
}

const result = limitFunctionCallCount(callBack, 3);

result.invokeFn();
result.invokeFn();
result.invokeFn();
result.invokeFn();
