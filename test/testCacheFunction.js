const cacheFunction = require("../cacheFunction.js");

const callBack = (element) =>
  console.log("New value added to the cache: " + element);
const returnedFunction = cacheFunction(callBack);

returnedFunction(10);
returnedFunction(100);
returnedFunction(10, 100, 200);
returnedFunction(10, 100, 200);
returnedFunction(10, 20);
