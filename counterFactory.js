function counterFactory() {
  let count = 0;

  return {
    increment: function () {
      return (count += 1);
    },
    decrement: function () {
      return (count -= 1);
    },
  };
}

module.exports = counterFactory;
