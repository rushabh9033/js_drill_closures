function limitFunctionCallCount(cb, n) {
  let counter = 0;
  function invokeFn() {
    if (counter == n) {
      console.log("Callback function call limit exceeded.");
    } else {
      counter += 1;
      cb();
    }
  }
  return { invokeFn };
}

module.exports = limitFunctionCallCount;
