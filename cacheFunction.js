function cacheFunction(cb) {
  let cache = {};

  return function (...arg) {
    if (cache[arg]) {
      console.log("This arguments fetching from the cache", cache);
    } else {
      cache[arg] = cb(arg);
      cache[arg] = arg;
    }
  };
}

module.exports = cacheFunction;
